#!/bin/bash

now=$(date +%s)

cat << EOF | redis-cli -h ${HOSTNAME_SERVER} -p ${PORT_NUMBER} | sed '1d' > AlarmDefinitions_${now}.log
select 1
KEYS '*AlarmDefinitions*'
EOF


while read keyname; do
StateSetValue=$(cat << EOF | redis-cli -h ${HOSTNAME_SERVER} -p ${PORT_NUMBER} |sed '1d' | jq '.stateSet.set'
select 1
get "${keyname}"
EOF
)
StateAckValue=$(cat << EOF | redis-cli -h ${HOSTNAME_SERVER} -p ${PORT_NUMBER} |sed '1d' | jq '.stateAck.acked'
select 1
get "${keyname}"
EOF
)
TimeStampValue=$(cat << EOF | redis-cli -h ${HOSTNAME_SERVER} -p ${PORT_NUMBER} |sed '1d' | jq '.stateSet.setTimestamp' | sed 's/\"//g'
select 1
get "${keyname}"
EOF
)

key1TimeSeconds=$(date -d "${TimeStampValue=}" +%s)
key1AgeSeconds=$(( now - key1TimeSeconds ))
key1AgeDays=$(( key1AgeSeconds / (60 * 60 * 24) ))


if [ ${StateSetValue} == "false" -a ${StateAckValue} == "true" -a ${key1AgeDays} -gt 30 ];then
   echo "Next key should be deleted"
   echo "${keyname}"
   echo "StateSetValue: ${StateSetValue}"
   echo "StateAckValue: ${StateAckValue}"
   echo "TimeStampValue: ${TimeStampValue}  - AgeDays: ${key1AgeDays}"
   echo "Deleting key..."
   cat << EOF | redis-cli -h ${HOSTNAME_SERVER} -p ${PORT_NUMBER}
select 1
del "${keyname}"
EOF
   echo ""
fi

done < AlarmDefinitions_${now}.log


kubectl rollout restart deploy/cp-cop-alarms -n cp
